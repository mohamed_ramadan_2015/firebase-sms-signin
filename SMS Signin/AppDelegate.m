//
//  AppDelegate.m
//  SMS Signin
//
//  Created by M R on 7/10/17.
//  Copyright © 2017 M R. All rights reserved.
//

#import "AppDelegate.h"
@import Firebase;
@import FirebaseAuth;
//#import <FirebaseAuth/FIRAuth.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    UIUserNotificationSettings *notificationSettings=[UIUserNotificationSettings
                                                      settingsForTypes:UIUserNotificationTypeAlert| UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil];
    
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [FIRApp configure];
    

    return YES;
}


-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"errooooor %@", error);
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Pass device token to auth.
    
    NSLog(@"Device Token : %@", deviceToken);
    NSMutableString *string = [[NSMutableString alloc]init];
    int length = [deviceToken length];
    char const *bytes = [deviceToken bytes];
    
    for(int i=0; i<length; i++){
        [string appendString:[NSString stringWithFormat:@"%02.2hhx",bytes[i]]];
    }
    
    NSLog(@"device token %@",string);
    
    //[[FIRAuth auth] setAPNSToken:deviceToken type:FIRAuthAPNSTokenTypeProd];
    
    NSLog(@"APNToken: %@",[[FIRAuth auth] APNSToken]);
    
    //NSLog(@"device token %@",deviceToken);
    // Further handling of the device token if needed by the app.
}


- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)notification
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // Pass notification to auth and check if they can handle it.
    if ([[FIRAuth auth] canHandleNotification:notification]) {
        
        NSLog(@"notiiiiiiiii %@",notification);
        
        completionHandler(UIBackgroundFetchResultNoData);
        
        
        
        return;
    }
    
    
    // This notification is not auth related, developer should handle it.
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
