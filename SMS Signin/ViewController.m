//
//  ViewController.m
//  SMS Signin
//
//  Created by M R on 7/10/17.
//  Copyright © 2017 M R. All rights reserved.
//

#import "ViewController.h"

@import FirebaseAuth;

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet UITextField *codeTextfiels;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    
    
}

- (IBAction)verifyButtonClicked:(id)sender {
    
    NSString *phone = [NSString stringWithFormat:@"+2%@",[_phoneTextfield text]];
    
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:phone
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                if (error) {
                                                    
                                                    NSLog(@"mmmmmmmmm %@",error);
                                                    
                                                    
                                                    NSLog(@"aaaaaaaaaa");
                                                    //[self showMessagePrompt:error.localizedDescription];
                                                    return;
                                                }
                                                // Sign in using the verificationID and the code sent to the user
                                                // ...
                                                
                                                NSLog(@"llllllllll");
                                                
                                                
                                                [self saveVerificationID:verificationID];
                                                
                                                //[self verifyPhoneNumberWithCode:verificationID code:@"15426"];
                                                
                                                
                                                
                                            }];
}



-(void) saveVerificationID:(NSString *)verificationID{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:verificationID forKey:@"authVerificationID"];
    [defaults synchronize];
    
    NSString *verID = [defaults stringForKey:@"authVerificationID"];
    
    NSLog(@"VerID = %@",verID);
}

- (IBAction)submitCodeButtonClicked:(id)sender {
    NSString *code = [NSString stringWithFormat:@"%@",[_codeTextfiels text]];
    
    NSLog(@"SMS Code is: %@",[_codeTextfiels text]);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *verificationID = [defaults stringForKey:@"authVerificationID"];
    
    FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
                                     credentialWithVerificationID:verificationID
                                     verificationCode:code];
    
    NSLog(@"Credential: %@",credential);
    
    [self signInWithPhoneAuthCredential:credential];
}




-(void) verifyPhoneNumberWithCode:(NSString *)verificationID  code: (NSString *)code {
    FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
                                     credentialWithVerificationID:verificationID
                                     verificationCode:code];
    
    [self signInWithPhoneAuthCredential:credential];
    

}


-(void) signInWithPhoneAuthCredential:(FIRAuthCredential *) credential{
    [[FIRAuth auth] signInWithCredential:credential
                              completion:^(FIRUser *user, NSError *error) {
                                  if (error) {
                                      // ...
                                      NSLog(@"invalide code -_- ");
                                      NSLog(@"fffffff %@",error);
                                      return;
                                  }
                                  
                                  NSLog(@"User SignIn Successfully :D");
                                  NSLog(@"%@",user);
                                  NSLog(@"rrrrrrrr");
                                  // User successfully signed in. Get user data from the FIRUser object
                                  // ...
                              }];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
